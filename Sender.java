package Base;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

public class Sender {

	private String host="192.168.127.130";
	private int port=8080;
	private Socket socket;
	private static int stopWay=1;
	private final int NATURAL_STOP =1;
	private final int SUDDEN_STOP =2;
	private final int SOCKET_STOP =3;
	private final int OUTPUT_STOP =4;
	
	public static void main(String[] args) throws Exception {
		 System.out.println("请输入结束方式(1 表示正常关闭 2表示突然终止程序3表示关闭socket,然后关闭程序4表示关闭输出流,再结束程序");
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		stopWay= Integer.parseInt(reader.readLine());
		new Sender().send();
	} 
	
	public Sender() throws IOException {
		socket = new Socket(host,port);
	}
	
	public PrintWriter getWriter(Socket socket) throws IOException{
		return new PrintWriter(new PrintWriter(socket.getOutputStream()),true);
	}
	
	public void send()  throws Exception{
		PrintWriter writer = getWriter(socket);
		for(int i=0;i<20;i++){
			String msg= "hello_"+i;
			writer.println(msg);
			Thread.sleep(500);
			System.out.println("send:"+msg);
			if(i==2){
				if(stopWay==SUDDEN_STOP){
					System.out.println("突然终止程序");
					System.exit(0);
				}else if(stopWay==SOCKET_STOP){
					System.out.println("关闭socket");
					socket.close();
				}else if(stopWay==OUTPUT_STOP){
					socket.shutdownOutput();
					System.out.println("关闭输出流并终止程序");
					break;
				}
			}
		}
		if(stopWay==NATURAL_STOP){
			socket.close();
			System.out.println("正常关闭");
		}
	}	
}
