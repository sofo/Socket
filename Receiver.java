package Future;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

public class Receiver {
	private int port=8080;
	private ServerSocket serverSocket;
	private static int stopWay=1;
	private final int NATURAL_STOP =1;
	private final int SUDDEN_STOP =2;
	private final int INPUT_STOP =4;
	private final int SERVERSOCKET_STOP =5;
	
	public static void main(String[] args) throws Exception {
		System.out.println("请输入结束方式(1 表示正常关闭 2表示突然终止程序3表示关闭socket,然后关闭程序4表示关闭输出流,再结束程序5表示关闭serverstop再结束程序)");
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		stopWay= Integer.parseInt(reader.readLine());
		new Receiver().receive();
	} 
	
	public Receiver() throws IOException {
		serverSocket = new ServerSocket(port);
	}
	
	public BufferedReader getReader(Socket socket) throws IOException{
		return new BufferedReader(new InputStreamReader(socket.getInputStream()));
	}
	
	public void receive()  throws Exception{
		Socket socket=null;
		socket = serverSocket.accept();
		BufferedReader reader = getReader(socket);
		for(int i=0;i<20;i++){
			String msg= reader.readLine();
			System.out.println("receiver:"+msg);
			Thread.sleep(1000);
			if(i==2){
				if(stopWay==SUDDEN_STOP){
					System.out.println("突然终止程序");
					System.exit(0);
				}else if(stopWay==SERVERSOCKET_STOP){
					System.out.println("关闭serverSocket并终止程序");
					serverSocket.close();
				}else if(stopWay==INPUT_STOP){
					socket.shutdownInput();
					System.out.println("关闭输入流并终止程序");
					break;
				}
			}
		}
		if(stopWay==NATURAL_STOP){
			socket.close();
			serverSocket.close();
			System.out.println("正常关闭");
		}
	}	
}

